#pragma SDS data copy(src[0:(SRC_RANGE/fragments)], dest[0:(DEST_RANGE/fragments)])
#pragma SDS data access_pattern(src:SEQUENTIAL, dest:SEQUENTIAL)
/*---------------------------------------------------------------------------*/
void I_Stretch2x_HW(byte src[SRC_RANGE], byte dest[DEST_RANGE], byte fragments)
{
    int y, x, z;

    byte temp_value;

    // byte line_buffer[LINES][SCREENWIDTH];
    byte line_buffer[SCREENWIDTH];

    /* For every 5 lines of src_buffer, 12 lines are written to dest_buffer */
    // (200 -> 960)
    for_height:for (y=0; y<(SCREENHEIGHT/fragments); y += 5)
    {
        /* Store the data in line 0 */
        src_line0:for (x=0; x<SCREENWIDTH; x++)
        {
            line_buffer[x] = *src;
            src++;
        }

        /* Write 5 times the line */
        line_0: for (z=0; z<3; z++)
	    {
	        write_line0:for (x=0; x<SCREENWIDTH; x++)
	        {
	        	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line_0


        /* Store the data in line 1 */
        src_line1:for (x=0; x<SCREENWIDTH; x++)
        {
            line_buffer[x] = *src;
            src++;
        }

        /* Write 5 times line 1 */
        line_1:for (z=0; z<3; z++)
        {
        	write_line1:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
        } // line 1


        /* Store the data in line 2 */
        src_line2:for (x=0; x<SCREENWIDTH; x++)
	    {
            line_buffer[x] = *src;
            src++;
	    }

        /* Write 5 times line 2 */
	    line_2:for (z=0; z<2; z++)
	    {
	        write_line2:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line 2


        /* Store the data in line 3 */
	    src_line3:for (x=0; x<SCREENWIDTH; x++)
	    {
            line_buffer[x] = *src;
            src++;
	    }

        /* Write 5 times line 3 */
	    line_3:for (z=0; z<2; z++)
	    {
	        write_line3:for (x=0; x<SCREENWIDTH; x++)
            {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line_3


        /* Store the data in line 4 */
	    src_line4:for (x=0; x<SCREENWIDTH; x++)
	    {
            line_buffer[x] = *src;
            src++;
	    } // store_line_4

        /* Write 4 times line 4 */
        line_4:for (z=0; z<2; z++)
	    {
        	write_line4:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }

         } // line_4

    } // loop_height
}